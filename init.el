;; .emacs.d/init.el

;; ===================================
;; MELPA Package Support
;; ===================================

;; shell commands: python-black, fd, ag
;; python packages: pyright (python lsp), debugpy (python dap)

;;ptvsd>=4.2 (python dap)

;; Adds the Melpa archive to the list of available repositories
;;(setq package-archives
;;      '(("melpa" . "https://melpa.org/packages/")
;;	("elpa" . "https://elpa.gnu.org/packages/")))

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/"))

;; Initializes the package infrastructure
(package-initialize)

(setq use-package-always-ensure t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

(use-package undo-fu
  :ensure t
  :demand t)

(use-package bind-key
  :ensure t
  :defer t)

(use-package evil
  :ensure t
  :demand t
  :after (:all bind-key undo-fu)
  :bind (("<escape>" . keyboard-escape-quit))
  :init
  (setq evil-search-module 'evil-search)
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)
  :config
  (bind-key* [remap evil-quit] 'kill-this-buffer)
  (setq evil-ex-search-highlight-all nil)
  (evil-mode 1))

(use-package evil-collection
  :ensure t
  :demand t
  :after evil
  :config
  (setq evil-want-integration t)
  (evil-collection-init))



;;get initial window
;;toggle symbols
;;get symbols window
;;select initial window
;;move far right
;;split to the right
;;move right
;;select symbols buffer
;;close initial symbols window
;;select initial window


(defun move-far-right ()
  (interactive)
  (setq w1 (selected-window))
  (ignore-errors (evil-window-right 1))
  (setq w2 (selected-window))
  (unless (eq w1 w2)
    (setq w1 (selected-window))
    (ignore-errors (evil-window-right 1))
    (setq w2 (selected-window))))
  


(defun toggle-object-browser ()
  (interactive)
  (setq ob-buffer (get-buffer "*LSP Symbols List*"))
  (setq ob-win nil)
  (unless (eq ob-buffer nil)
    (setq ob-win (get-buffer-window ob-buffer)))
  (cond
   ((or (eq ob-buffer nil) (eq ob-win nil)) (progn
                                              (setq ini-win (selected-window))
                                              (lsp-treemacs-symbols)
                                              (delete-window)
                                              (setq ob-buffer (get-buffer "*LSP Symbols List*"))
                                              (select-window ini-win)
                                              (move-far-right)
                                              (split-window-right)
                                              (evil-window-right 1)
                                              (switch-to-buffer ob-buffer)
                                              (evil-window-set-width treemacs-width)
                                              (select-window ini-win)
                                              ))
   (t (delete-window ob-win))))

(use-package evil-leader
  :ensure t
  :demand t
  :after evil-collection
  :init
  (setq evil-leader/in-all-states t)
  :config
  (global-evil-leader-mode t)
  (evil-leader/set-leader "\\")
  (evil-leader/set-key
    "q" 'kill-buffer
    "c" 'evil-window-delete
    "o" 'toggle-object-browser
    "t" (lambda () (interactive) (setq win (selected-window)) (treemacs) (when (window-live-p win) (select-window win)))
    "v" (lambda () (interactive) (split-window-right) (evil-window-right 1))
    "h" (lambda () (interactive) (split-window-below) (evil-window-down 1)))
  (evil-mode -1)
  (evil-mode 1))

    ;;"f" 'helm-occur

(use-package better-defaults
  :ensure t
  :demand t
  :config
  (setq ido-everywhere nil)
  (ido-mode -1))

(use-package blacken
  :ensure t
  :defer t)

(use-package material-theme
  :ensure t
  :demand t
  :config
  (load-theme 'material t))

(use-package magit
  :ensure t
  :defer t)

(use-package lsp-mode
  :ensure t
  :defer t
  :commands
  (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-pyright
  :ensure t
  :defer t)

(use-package dap-mode
  :ensure t
  :defer t)
;;  :config
;;  (add-to-list 'load-path (file-name-directory (locate-library "dap-mode"))))

(use-package dap-python
  :after dap-mode
  :ensure nil
  :defer t
  :init
  (add-to-list 'load-path (file-name-directory (locate-library "dap-mode")))
  :config
  (setq dap-python-debugger 'debugpy)
  (dap-mode 1)
  (dap-ui-mode 1)
  ;; enables mouse hover support
  (dap-tooltip-mode 1)
  ;; use tooltips for mouse hover
  ;; if it is not enabled `dap-mode' will use the minibuffer.
  (tooltip-mode 1)
  ;; displays floating panel with debug buttons
  ;; requies emacs 26+
  (dap-ui-controls-mode 1))


(use-package company
  :ensure t
  :defer t
  :config
  (setq company-require-match t)
  (setq company-minimum-prefix-length 0)
  :bind (:map company-active-map
    ("C-n" . company-select-next)
    ("C-p" . company-select-previous)
    ("<tab>" . company-complete-selection)))

(use-package python-mode
  :ensure t
  :defer t
  :mode "\\.py\\'"
  :hook
  (python-mode . (lambda () (require 'lsp-pyright) (lsp-deferred)))
  (python-mode . (lambda () (require 'dap-python)))
  (python-mode . (lambda () (company-mode 1))))



  ;;:hook (python-mode . (lambda () (company-mode 1)))

;;(define-key company-active-map (kbd "<tab>") 'company-complete-selection)

;;(with-eval-after-load 'lsp-mode
;;  (define-key lsp-mode-map (kbd "TAB") #'company-indent-or-complete-common))

(use-package which-key
  :ensure t
  :demand t
  :config
  (which-key-mode 1))

(use-package centaur-tabs
  :after (:all evil)
  :ensure t
  :demand t
  :config
  (centaur-tabs-mode 1)
  (define-key evil-normal-state-map (kbd "<tab>") 'centaur-tabs-forward)
  (define-key evil-normal-state-map (kbd "<backtab>") 'centaur-tabs-backward)
  (define-key evil-normal-state-map (kbd "g t") 'centaur-tabs-forward)
  (define-key evil-normal-state-map (kbd "g T") 'centaur-tabs-backward)
  (define-key evil-normal-state-map (kbd "g p") 'centaur-tabs-forward-group)
  (define-key evil-normal-state-map (kbd "g P") 'centaur-tabs-backward-group)
  (setq centaur-tabs-style "bar")
  (setq centaur-tabs-height 20)
  (setq centaur-tabs-set-close-button nil)
  (setq centaur-tabs-set-modified-marker t)
  (setq centaur-tabs-modified-marker "*")
  (setq centaur-tabs-cycle-scope 'tabs)
  ;;(centaur-tabs-group-by-projectile-project)
  )

(defun centaur-tabs-buffer-groups ()
  (list default-directory))

(defun centaur-tabs-hide-tab (x)
  "Do no to show buffer X in tabs."
  (let ((name (format "%s" x)))
    (or
     (window-dedicated-p (selected-window))
     (string-prefix-p "*Treemacs-Scoped-Buffer" name)
     (string-prefix-p "*Flymake" name)
     (string-prefix-p "*lsp" name)
     (string-prefix-p "*pyright" name)
     (string-prefix-p "*Messages" name)
     (string-prefix-p "*scratch" name)
     (string-prefix-p "*LSP" name)
     (string-prefix-p "magit" name)
     ;;(string-match-p (regexp-quote "*") name)
     )))

;;(defun centaur-tabs-buffer-groups ()
;;  (list
;;    (cond
;;      ((or (string-equal "*" (substring (buffer-name) 0 1)) (memq major-mode '(
;;        magit-process-mode
;;        magit-status-mode
;;        magit-diff-mode
;;        magit-log-mode
;;        magit-file-mode
;;        magit-blob-mode
;;        magit-blame-mode)))
;;      "Emacs")
;;    ((derived-mode-p 'prog-mode) "Editing")
;;    ((derived-mode-p 'dired-mode) "Dired")
;;    ((memq major-mode '(helpful-mode help-mode)) "Help")
;;    ((memq major-mode '(
;;      org-mode
;;      org-agenda-clockreport-mode
;;      org-src-mode
;;      org-agenda-mode
;;      org-beamer-mode
;;      org-indent-mode
;;      org-bullets-mode
;;      org-cdlatex-mode
;;      org-agenda-log-mode
;;      diary-mode))
;;    "OrgMode")
;;    (t (centaur-tabs-get-group-name (current-buffer))))))

;;    (t (centaur-tabs-get-group-name (current-buffer))))))
;;    (t (centaur-tabs-get-group-name (current-buffer))))))
;;    (t (centaur-tabs-get-group-name (current-buffer))))))
;;    (t (centaur-tabs-get-group-name (current-buffer))))))
;;    (t (centaur-tabs-get-group-name (current-buffer))))))
;;    (t (centaur-tabs-get-group-name (current-buffer))))))

(use-package treemacs
  :ensure t
  :demand t)

(use-package treemacs-evil
  :after (:all evil treemacs)
  :ensure t
  :demand t)

;;(use-package tagbar
;;  :straight t
;;  :ensure t
;;  :defer t)

;;(use-package helm
;;  :after (:all bind-key evil)
;;  :ensure t
;;  :demand t
;;  :init
;;  :config
;;  (helm-mode 1)
;;  (bind-key* [remap execute-extended-command] 'helm-M-x)
;;  (bind-key* [remap bookmark-jump] 'helm-filtered-bookmarks)
;;  (bind-key* [remap find-file] 'helm-find-files)
;;  (bind-key* [remap isearch-forward-regexp] 'helm-occur)
;;  (bind-key* (kbd "M-y") 'helm-show-kill-ring)
;;  (bind-key* [remap switch-to-buffer] 'helm-mini)
;;  (bind-key* (kbd "C-c h") 'helm-command-prefix)
;;  (global-unset-key (kbd "C-x c"))
;;  (setq helm-split-window-inside-p t)
;;  (setq helm-split-window-default-side 'below))

;;(use-package projectile
;;  :ensure t
;;  :init
;;  (projectile-mode +1)
;;  :bind (:map projectile-mode-map
;;              ("C-c p" . projectile-command-map)))
  

(setq inhibit-startup-message t)
(global-linum-mode t)

(setq make-backup-files t)
(setq backup-directory-alist `(("." . "~/emacs_backup_folder")))
(setq backup-by-copying t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

;; Makes *scratch* empty.
(setq initial-scratch-message "")
;; No more typing the whole yes or no. Just y or n will do.
(fset 'yes-or-no-p 'y-or-n-p)
;; Maximize the screen on startup.
(add-hook 'emacs-startup-hook 'toggle-frame-maximized)


(setq use-short-answers t)
(setq confirm-nonexistent-file-or-buffer nil)
(setq ido-create-new-buffer 'always)
(setq inhibit-startup-message t
      inhibit-startup-echo-area-message t)
(setq kill-buffer-query-functions
  (remq 'process-kill-buffer-query-function
         kill-buffer-query-functions))


(bind-key* [remap kill-buffer] 'kill-this-buffer)
(bind-key* "C-h" (lambda () (interactive) (evil-window-left 1)))
(bind-key* "C-j" (lambda () (interactive) (evil-window-down 1)))
(bind-key* "C-k" (lambda () (interactive) (evil-window-up 1)))
(bind-key* "C-l" (lambda () (interactive) (evil-window-right 1)))

;;NOTE - evil-ex-commands is the variable which stores ex command bindings


(setq display-line-numbers-type 'relative) 
(global-display-line-numbers-mode 1)
;;(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(add-hook 'buffer-list-update-hook (lambda () (interactive) (linum-mode -1)))
;;(add-hook 'emacs-startup-hook (lambda () (interactive) (linum-mode -1)))

(defun def ()
  "Jump to definition of symbol."
  (interactive)
  (lsp-find-definition))

(defun ws ()
  "Write and source the file."
  (interactive)
  (save-buffer)
  (load-file (buffer-file-name)))

(show-paren-mode 0)


